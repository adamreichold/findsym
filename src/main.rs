use std::env::args;
use std::fs::File;
use std::io::{Error, ErrorKind, Read, Result};

use rayon::prelude::*;

use goblin::Object;
use walkdir::WalkDir;

fn main() -> Result<()> {
    let args = args().skip(1).collect::<Vec<_>>();
    let [dir, sym] = &args[..] else {
        return Err(Error::new(ErrorKind::Other, "usage: dir symbol"));
    };

    WalkDir::new(dir)
        .into_iter()
        .par_bridge()
        .try_for_each_init(Vec::new, |buffer, entry| {
            let entry = entry.unwrap();
            if entry.file_type().is_dir() {
                return Ok(());
            }

            if !entry
                .file_name()
                .to_str()
                .map_or(false, |file_name| file_name.contains(".so"))
            {
                return Ok(());
            }

            buffer.clear();
            File::open(entry.path())?.read_to_end(buffer)?;

            if let Ok(Object::Elf(elf)) = Object::parse(buffer) {
                for dynsym in elf.dynsyms.iter() {
                    if dynsym.is_import() {
                        continue;
                    }

                    if let Some(dynsym) = elf.dynstrtab.get_at(dynsym.st_name) {
                        if dynsym.contains(sym) {
                            println!("{}", entry.path().display());
                            return Ok(());
                        }
                    }
                }
            }

            Ok(())
        })
}
